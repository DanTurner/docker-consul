#!/bin/bash

set -e
export ADVERTISE_IP=$(echo $TUTUM_IP_ADDRESS | cut -d/ -f1)
if [ -z "$ADVERTISE_IP" ]; then 
    export ADVERTISE_IP=0.0.0.0
fi

/bin/consul agent -advertise=$ADVERTISE_IP -client=$ADVERTISE_IP -data-dir=/data "$@"
